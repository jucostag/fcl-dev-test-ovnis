<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category RealMedia Tags
 * @package  RealMediaTags
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Metabox for the realmediaurl.
 */
function rmtRegisterMetaBox()
{

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_rmt_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'realmediaurl',
		'title'         => __( 'Informações da URL', 'cmb2' ),
		'object_types'  => array( 'realmediaurl', ),
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'URL', 'cmb2' ),
		'desc' => __( 'Coloque aqui a URL da página, ou grupo de URLs, nas quais o script será inserido no header com os parâmetros definidos abaixo em sitepage e listpos. </br></br>Ex.: videos/*, category/featured.', 'cmb2' ),
		'id'   => $prefix . 'url',
		'type' => 'text_medium',
		'repeatable' => true,
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Sitepage', 'cmb2' ),
		'desc' => __( 'Equivalente a variável OAS_sitepage, disponível no script da Real Media.', 'cmb2' ),
		'id'   => $prefix . 'url_sitepage',
		'type' => 'text_medium',
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Listpos', 'cmb2' ),
		'desc' => __( 'Equivalente a variável OAS_listpos, disponível no script da Real Media.', 'cmb2' ),
		'id'   => $prefix . 'url_listpos',
		'type' => 'text_medium',
	) );
}

add_action( 'cmb2_init', 'rmtRegisterMetaBox' );
