##Abduction

Este repositório contém o esquema de abdução utilizado pelos OVNIs para encontrar o grupo de desenvolvedores que será levado, a cada cometa.

#### Classes

No package **Abduction**, podem ser encontradas as classes abaixo, responsáveis pelos principais cálculos do esquema de abdução.

###### AbductionScheme

Esta é a classe responsável por lidar com o cálculo do esquema de abdução. 

**calculate()**

O método **calculate()** é o responsável por calcular o resto da divisão dos números das entidades. Utiliza três parâmetros, sendo eles, o número do cometa, o número do grupo de desenvolvedores, e o número pelo qual eles serão divididos e o resto extraído. Ele deve retornar ***true***, caso o grupo passado no segundo parâmetro seja levado pelo cometa passado no primeiro parâmetro, ou, ***false***, caso o grupo não seja levado.

###### AbductionEntity

Esta é a classe responsável por lidar com as entidades presentes na abdução de grupos, ou seja, o OVNI e o grupo de desenvolvedores. Ambos utilizam os mesmos métodos para cálculo de seus números. Esta classe contém um construtor, para coletar o nome da entidade que será calculada, e um método get, para recuperar a string, quando necessário.

**calculateStringLetters()**

O método **calculateStringLetters()** é responsável por calcular individualmente o número de cada entidade. Não recebe nenhum parâmetro pois utiliza-se da mesma string passada no construtor. Este método limpa a string de espaços em branco e a padroniza para letras maiúsculas, e após este processo, a string é quebrada, inserindo cada letra em um item do array **$theEntityLetters**. Após isso, este array é lido, comparando cada letra com o alfabeto, encontrando a posição correspondente de cada letra presente na string, e posteriormente multiplicando cada uma, retornando o resultado desejado.

###### Para executar

Para executar, basta utilizar a linha de comando, e rodar o arquivo **abduct.php**, que lhe retornará automaticamente qual grupo não será levado, caso exista algum.
