<?php

namespace Abduction;

/**
 * AbductionScheme class
 * 
 * Convert and calculate the string's letters values. The string can contain one or more words.
 * 
 * @author Juliana Gonçalves <juliana.goncosta@gmail.com>
 * @package Abduction
 */
class AbductionScheme
{
	/**
	 * Calculate the rest of the division for the comet and group strings.
	 * 
	 * @param  AbductableInterface 	$theComet	The comet name.
	 * @param  AbductableInterface 	$theGroup	The group name.
	 * @param  int 					$theDivisionValue	The division value, to calculate the remainder.
	 * @return boolean 				The result of the calculation, telling if the abduction will be made or not.
	 */
	public function calculate( $theComet, $theGroup, $theDivisionValue )
	{

		/**
		 * The remainder of comet's name division by $theDivisionValue
		 * @var int
		 */
		$theCometRemainder = gmp_div_r( $theComet, $theDivisionValue );
		/**
		 * The remainder of group's name division by $theDivisionValue
		 * @var int
		 */
		$theGroupRemainder = gmp_div_r( $theGroup, $theDivisionValue );

		if ($theCometRemainder == $theGroupRemainder) 
		{
			return true;
		}
		else
		{
			return false; 
		}

	}

}