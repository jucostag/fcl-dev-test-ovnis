<?php

namespace Abduction;

/**
 * AbductionEntity class
 * 
 * Handle the entity name's letters. The string can contain one or more words.
 * 
 * @author Juliana Gonçalves <juliana.goncosta@gmail.com>
 * @package Abduction
 */

class AbductionEntity
{
	/**
	 * The entity name.
	 * @var string
	 */
	public $theEntity;

	/**
	 * The entity constructor.
	 * @param string $theString The name for the entity.
	 */
	public function __construct($theString)
	{
		$this->theEntity = $theString;
	}

	/**
	 * Get the given name of the entity.
	 * @return string The entity name.
	 */
	public function getAbductionEntity()
	{
		return $this->theEntity;
	}

	/**
	 * Sum each letter's alphabet position of the given entity name.
	 * 
	 * @return int 	The sum of all the entity name's letters, based on the alphabet's position.
	 */
	public function calculateStringLetters()
	{
		/**
		 * The entire alphabet.
		 * @var array
		 */
		$theAlphabet = range('A', 'Z');

		/**
		 * Remove white spaces from the string, standardizes the letters 
		 * to uppercase, and build the array, containing the string's letters.
		 * @var string
		 */
		$theEntityLetters = str_split( trim( strtoupper( $this->theEntity ) ) );

		/**
		 * Initialize a counter to sum the string's letters positions (keys) on theAlphabet array.
		 * @var int
		 */
		$theLettersSum = 0;

		foreach($theEntityLetters as $singleLetter){
			$theLettersSum *= intval( array_search( $singleLetter , $theAlphabet ) );
		}

		return $theLettersSum;

	}

}