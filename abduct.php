<?php
/**
 * Set charset to UTF-8.
 */
header('Content-Type: text/html; charset=UTF-8');

/**
 * Calls the Abduction package.
 */
require __DIR__ . '/vendor/autoload.php';

use Abduction\AbductionScheme;
use Abduction\AbductionEntity;

/**
 * Initialize the AbductionScheme object, to calculate the abduction.
 * @var AbductionScheme
 */
$theScheme = new AbductionScheme;

/**
 * Set the comets and respective groups.
 * @var array
 */
$theCometsAndGroups = array('halley' => 'amarelo', 'encke' => 'vermelho', 'wolf' => 'preto', 'kushida' => 'azul');

/**
 * Initialize a loop to read and calculate the comets and developers groups.
 */
// foreach($theCometsAndGroups as $comet => $group)
// {
	/**
	 * Initialize the AbductionEntity object, to receive the comet name.
	 * @var AbductionEntity
	 */
	$theComet = new AbductionEntity('halley');

	/**
	 * Initialize the AbductionEntity object, to receive the group name.
	 * @var AbductionEntity
	 */
	$theGroup = new AbductionEntity('amarelo');

	var_dump($theComet);
	var_dump($theGroup);
	var_dump($theComet->calculateStringLetters());
	var_dump($theGroup->calculateStringLetters());
	var_dump(gmp_div_r( $theComet->calculateStringLetters(), 45 ));
	var_dump(gmp_div_r( $theGroup->calculateStringLetters(), 45 ));

	/**
	 * Calculate the abduction entities using the scheme method 'calculate', to show the result for the comet / group combination.
	 * @var boolean.
	 */
	$theAbduction = $theScheme->calculate( $theComet->calculateStringLetters() , $theGroup->calculateStringLetters() , 45 );

	if (!$theAbduction) 
	{
		echo 'O Grupo '.$theGroup->getAbductionEntity().' NÃO será levado pelo cometa '.$theComet->getAbductionEntity(); 
	}
// }